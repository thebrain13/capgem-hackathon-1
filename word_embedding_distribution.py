import spacy
import gensim
from os import listdir
from os.path import isfile, join
import os
import nltk
import numpy as np
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
#from sklearn.datasets import load_digits
from sklearn.neighbors import KernelDensity
#from sklearn.decomposition import PCA
from sklearn.model_selection import GridSearchCV
from scipy.stats import entropy
from bokeh.plotting import figure, output_file, show, ColumnDataSource
from bokeh.models import HoverTool

class WordEmbeddingDistribution(object):
    """Some docstring text here describing this class...
    """

    def __init__(self, we_model='word2vec'):
        self.nlp = spacy.load('en', vectors='en_glove_cc_300_1m')
        self.we_model = we_model

    @staticmethod
    def word_embedding_distribution(input_text, embedding_dimensions=100, plot=True):
        word_tokens = nltk.word_tokenize(input_text)
        list_of_tokenized_sentences = []
        sentences = nltk.sent_tokenize(input_text) #this gives us a list of sentences
        for sentence in sentences:
            tokenized_sentence = nltk.word_tokenize(sentence)
            list_of_tokenized_sentences.append(tokenized_sentence)

        model = gensim.models.Word2Vec(list_of_tokenized_sentences, size=embedding_dimensions, window=3, min_count=1, workers=4)

        # print (model.wv.vocab)
        document_average_word_embedding = np.zeros(embedding_dimensions)

        # record value of each WEV's dimension, for all words
        document_wed = [[]] * embedding_dimensions

        for token in word_tokens:
            # document_average_word_embedding = document_average_word_embedding + tokenized_sentence
            word_embedding_for_token = model.wv[token]

            #for each token, go through every dimension of it and record in the appropriate list of document_wed
            for i in range(embedding_dimensions):
                document_wed[i-1] = document_wed[i-1] + [word_embedding_for_token[i-1]]
            # print ("token's WE = ", word_embedding_for_token)
            document_average_word_embedding = document_average_word_embedding + word_embedding_for_token

        document_wed = np.array(document_wed)
        X_embedded = TSNE(n_components=3).fit_transform(np.transpose(document_wed))

        return np.transpose(document_wed), X_embedded
