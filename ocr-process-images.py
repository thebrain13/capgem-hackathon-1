#!/usr/bin/env python

# [START import_libraries]
from __future__ import division

# for string matching and regexes
import re
import sys
import os
import io
import numpy as np
import json

# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision import types

from word_embedding_distribution import WordEmbeddingDistribution

def main():
    fileData = {}
    fileData['texts'] = {}
    # fileData['textsBoxes'] = {}
    fileData['textWEs'] = {}
    fileData['textTSNEs'] = {}
    # Instantiates a client
    client = vision.ImageAnnotatorClient()

    for filecount, filename in enumerate(os.listdir('images')):
        if filename.endswith(".jpg"):
            print("Processing file: ", filecount)
            # print("filename = ", filename)
            # The name of the image file to annotate
            file_name = os.path.join(
                os.path.dirname(__file__),
                'images/' + filename)

            # Loads the image into memory
            with io.open(file_name, 'rb') as image_file:
                content = image_file.read()

            image = types.Image(content=content)

            # Performs label detection on the image file
            response = client.text_detection(image=image)
            texts  = response.text_annotations
            entireText = ""
            # count = 0
            for count, text in enumerate(texts[1:]):
                fileData['texts'][count] = text.description
                # fileData['textsBoxes'][count] = text.bounding_poly
                # print("text.description = ", text.description)
                entireText = entireText + " " + text.description

            fileData['entireText'] = entireText
            wed = WordEmbeddingDistribution()
            try:
                wed, X_embedded = wed.word_embedding_distribution(entireText, embedding_dimensions = 100, plot=False)
            except:
                print("OCR didn't find any words :-(")
                continue

            # print("wed.shape = ", wed.shape)
            # print("X_embedded.shape = ", X_embedded.shape)
            fileData['textWEs'] = wed.tolist()
            fileData['textTSNEs'] = X_embedded.tolist()

            json_data = json.dumps(fileData)
            with open("json/" + filename.split(".")[0] + ".json", 'w') as outfile:
                json.dump(json_data, outfile)

        else:
            print("---> Skipping non-JPG file...")

if __name__ == '__main__':
    main()
